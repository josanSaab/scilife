<p align="center"><a href="https://aspireapp.com/" target="_blank"><img src="https://www.scilife.io/hubfs/Scilife-logo-100.svg" width="400"></a></p>

## About Scilife Task
Importing CSV through two steps.
A) Validation
B) Processing

## Integrating Scilife

Clone the application at your local by simply using this command <b>git clone git clone https://josanSaab@bitbucket.org/josanSaab/scilife.git</b>. After cloning is done setup your .env file as per you choice. The .env file is already at it's place as you can use the same configurations.(Please update the port and unix_socket in .env file)

After the setup is done simply hit the command <b>php artisan migrate</b> to populate your database for the required details on your local setup.

Next, run <b>php artisan websockets:serve</b>
</b>

.gitignore file was commented to put all the stuff on bitbucket like composer and npm. Simply clone the database and 

Note: This task was done as soon as possible as it could be done. The task can be more optimized.


<b>HAPPY CODING!</b>
