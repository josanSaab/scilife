<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\CsvImport;
use App\Models\Csv;
use Illuminate\Support\Facades\DB;

class CsvController extends Controller
{
    /**
     * @method validateCsv
     * @param $request
     * @response validation log
     *
     */
    public function validateCsv(Request $request)
    {
        if($request->ajax())
        {
            sleep(2);
            $log = [];
            $totalRecords = $request->totalRecords;
            $exceRecord   = $request->exec;
            $csvRecord = (object) $request->csv_data;
            $log = ['total' => $totalRecords, 'exec' => $exceRecord, 'currentRow' => $csvRecord, 'action' => 'validation'];
            if(empty($csvRecord->email) && !isset($csvRecord->email) || empty($csvRecord->first_name) && !isset($csvRecord->first_name) || empty($csvRecord->last_name) && !isset($csvRecord->last_name))
            {
                $log[] = ['validFail'=> $csvRecord];
            }else {
                $log[] = ['pass' => $csvRecord];
            }
            event(new CsvImport($log));
            return response()->json($log);
        }
    }

    /**
     * Inserting the pass logs while validation
     * @method validateCsv
     * @param $request
     * @response status success json
     *
     */
    public function saveCsv(Request $request)
    {
        if($request->ajax())
        {
            $log = [];
            $totalRecords = count($request->all());
            $exceRecord = 1;
            foreach ($request->all() as $key => $csv) {
                sleep(2);
                $csvInstance = new Csv;
                $csv = (object) $csv;
                $log = ['total' => $totalRecords, 'exec' => $exceRecord, 'currentRow' => $csv,'action' => 'process'];
                event(new CsvImport($log));
                $csvInstance->email = $csv->email;
                $csvInstance->person_prefix = $csv->person_prefix;
                $csvInstance->first_name = $csv->first_name;
                $csvInstance->last_name = $csv->last_name;
                $csvInstance->status = $csv->active;
                try {
                    DB::beginTransaction();
                    $csvInstance->save();
                    DB::commit();
                } catch (\Throwable $th) {
                    DB::rollBack();
                    // throw $th;
                }
                $exceRecord++;
            }
            return response()->json(['status' => 'success']);

        }
    }
}
