<?php

namespace App\Listeners;

use App\Event\CsvEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CsvListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Event\CsvEvent  $event
     * @return void
     */
    public function handle(CsvEvent $event)
    {
        return $event;
    }
}
