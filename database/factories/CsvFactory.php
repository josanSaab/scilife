<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CsvFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'email' => $this->faker->unique()->safeEmail,
            'person_prefix' => 'Mr.',
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'status' => '1',
        ];
    }
}
